import argparse
import pandas as pd
import matplotlib.pyplot as plt
import cv2

parser = argparse.ArgumentParser()
parser.add_argument('frame', type=str)
parser.add_argument('history', type=str)

args = parser.parse_args()

frame = cv2.imread(args.frame)
df = pd.read_csv(args.history)

in_AB = list()
ids = df['Object ID'].unique().tolist()

for vehicle in ids:
    data = df.loc[(df['Object ID'] == vehicle)]
    regions = data.Region.unique().tolist()
    if all([i in regions for i in (0, 1)]):
        in_AB.append(vehicle)

in_regions = df.loc[(df['Object ID'].isin(in_AB))]
plt.scatter(in_regions['Centroid X'], in_regions['Centroid Y'])
plt.imshow(frame)
plt.show()
print('objs in region A AND region B: {}'.format(len(in_AB)))
# 