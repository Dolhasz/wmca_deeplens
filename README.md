# WMCA Traffic Analysis Tools

This project is funded by the West Midlands Combined Authority 
and aims to develop software tools to exploit off-the-shelf computer vision hardware to perform traffic-analysis-related tasks, such as car counting, instance tracking, route mapping etc.

## Overview

This project provides implementations of instance tracking, data 
collection on object positions and visualisation tools. Modules contained in
the library work with inference output from object detectors. Feed
frames and corresponding predicted obeject coordinates to the engine using
the update methods.

### Features

The following functionalities are currently being prototyped:
* Vehicle counting,
* Vehicle tracking,
* Object direction traking,
* User inteface.

![Demo](docs/demo.gif)

### Inference server
Docker command:
docker run --runtime=nvidia -p 8500:8500 \
            --mount type=bind,source=/PATH/TO/wmca_deeplens/models,target=/models/obj_det \
            -e MODEL_NAME=obj_det -t tensorflow/serving:latest-gpu

## Contribute!

**THIS PROJECT IS OPEN SOURCE!**

Feel free to contribute, clone, create issues and do anything you feel might help.
We are actively looking to develop a community around this project, so if you are interested in
contributing in any way, please reach out to us via Slack:

```bit.ly/alanslack```

## Contacts

If you would like to get in touch with the project team, feel free to email:

mattia.colombo@bcu.ac.uk

or

alan.dolhasz@bcu.ac.uk


