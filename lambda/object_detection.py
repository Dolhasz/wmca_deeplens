import os
import threading
import json
import numpy as np
import awscam
import cv2
import greengrasssdk
import boto3
from datetime import datetime


CLASS_NAMES = {
    1: 'aeroplane', 2: 'bicycle', 3: 'bird', 4: 'boat', 5: 'bottle', 6: 'bus',
    7 : 'car', 8 : 'cat', 9 : 'chair', 10 : 'cow', 11 : 'dinning table',
    12 : 'dog', 13 : 'horse', 14 : 'motorbike', 15 : 'person', 
    16 : 'pottedplant', 17 : 'sheep', 18 : 'sofa', 19 : 'train',
    20 : 'tvmonitor'}
DETECTION_THRESHOLD = 0.25
ZOOM = 0.4
INPUT_RES = (300, 300)
height, width = INPUT_RES
MODEL_TYPE = 'ssd'
MODEL_PATH = '/opt/awscam/artifacts/mxnet_deploy_ssd_resnet50_300_FP16_FUSED.xml'


def infinite_infer_run():
    try:
        model = awscam.Model(MODEL_PATH, {'GPU': 1})
        publisher = MQTTPublisher(greengrasssdk.client('iot-data'))
        publisher.start()
        while True:
            local_vid = cv2.VideoCapture('/tmp/traffic_video.avi')
            while local_vid.isOpened():
                ret, frame = local_vid.read()
                # ret, frame = awscam.getLastFrame()
                if not ret:
                    raise Exception('Failed to get frame from the stream')
                frame_resize = cv2.resize(crop_im(frame, ZOOM), INPUT_RES)
                results = model.parseResult(MODEL_TYPE,
                                            model.doInference(frame_resize))
                infer = parse_inference(results[MODEL_TYPE])
                publisher.set_latest_prediction(json.dumps(infer))
                publisher.set_latest_frame(
                    cv2.imencode('.jpg', frame_resize)[1])
            local_vid.release()
    except Exception as ex:
        # TODO: Catch exceptions properly
        print(ex)



def crop_im(im, zoom):
    """ TODO: 1) Add zoom limits. 2) Add offset options to focus on a specific
    region """
    x1 = int(im.shape[0] * (1 - zoom))
    x2 = int(im.shape[1] * (1 - zoom))
    return im[int((im.shape[0] - x1)/2):int((im.shape[0] - x1)/2 + x1),
              int((im.shape[1] - x2)/2):int((im.shape[1] - x2)/2 + x2)]


def parse_inference(inference_results):
    """ Collects instances detected in a frame and formats output.
    """
    def set_instance(obj):
        obj_instance = {
            'BoundingBox': {
                'Top': obj['ymin'] / height,
                'Left': obj['xmin'] / width,
                'Height': obj['ymax'] / height - obj['ymin'] / height,
                'Width': obj['xmax'] / width - obj['xmin'] / width},
            'Confidence': obj['prob'] * 100,
            'Name': CLASS_NAMES[obj['label']]}
        return obj_instance

    out = {}
    out['Labels'] = {}
    for obj in inference_results:
        if obj['prob'] > DETECTION_THRESHOLD:
            obj_instance = set_instance(obj)
            if not CLASS_NAMES[obj['label']] in out['Labels'].keys():
                # Create another entry for new instances
                out['Labels'][CLASS_NAMES[obj['label']]] = []
                out['Labels'][CLASS_NAMES[obj['label']]].append(obj_instance)
            else:
                # Append to existing instances
                out['Labels'][CLASS_NAMES[obj['label']]].append(obj_instance)
    return out


class MQTTPublisher(threading.Thread):
    """ Thread for publishing output from the model to a MQTT topic.
    """
    def __init__(self, client):
        super(MQTTPublisher, self).__init__()
        self.client = client
        self.latest_frame = None
        self.latest_prediction = None
        self.iot_topic = '$aws/things/{}/infer'.format(
            os.environ['AWS_IOT_THING_NAME'])
        self.client.publish(
            topic=self.iot_topic,
            payload='Loading object detection model')
 
    def set_latest_frame(self, frame):
        self.latest_frame = frame

    def set_latest_prediction(self, j_preds):
        self.latest_prediction = j_preds

    def run(self):
        """ This writes data to a FIFO container waiting until a
        consumer reads on the other end. """
        while True:
            if (self.latest_frame is not None 
            and self.latest_prediction is not None):
                self.publish(self.latest_frame, self.latest_prediction)

    def publish(self, jpg, infer_dict):
        try:
            enc = jpg.tobytes()
            data = json.dumps({
                'Image': enc.encode('base64'), 'DL_Inference': infer_dict,
                'Time': datetime.now().strftime(r'%d/%m/%Y %Y:%M:%S')})
            self.client.publish(topic=self.iot_topic, payload=data)
        except Exception as exc:
            self.client.publish(
                topic=self.iot_topic,
                payload='Could not publish inference. Error: {}'.format(exc))
            print(exc)


infinite_infer_run()
