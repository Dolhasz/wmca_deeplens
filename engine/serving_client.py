from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
import json
import grpc
import tensorflow as tf
import cv2


CLASS_NAMES = './engine/classes.json'


class GRPCClient:
    def __init__(self, url='localhost', port=8500, model_name='obj_det',
        confidence=0.65):
        self.channel = grpc.insecure_channel('{}:{}'.format(url, int(port)))
        self.stub = prediction_service_pb2_grpc.PredictionServiceStub(
            self.channel)
        self.model_name = model_name
        self.confidence = float(confidence)
        with open(CLASS_NAMES, 'r') as file:
            self.classes = json.load(file)

    def predict(self, image):
        image = image.reshape(1, *image.shape[:2], 3)
        request = predict_pb2.PredictRequest()
        request.model_spec.name = self.model_name
        request.model_spec.signature_name = 'serving_default'
        request.inputs['inputs'].CopyFrom(
            tf.make_tensor_proto(image, shape=image.shape))
        return self.unpack_response(self.stub.Predict(request, 10))

    def unpack_response(self, response):
        preds = dict()
        preds['boxes'] = tf.make_ndarray(response.outputs['detection_boxes'])
        preds['classes'] = list(response.outputs['detection_classes'].float_val)
        preds['scores'] = list(response.outputs['detection_scores'].float_val)
        return preds

    def process_boxes(self, image, predictions, visualise=True):
        results = list()
        height, width = image.shape[:2]
        for key, score in enumerate(predictions['scores']):
            if score <= self.confidence:
                break
            name = self.classes[str(int(predictions['classes'][key]))]
            ymin, xmin, ymax, xmax = predictions['boxes'][0, key]
            pt1 = (int(xmin * width), int(ymin * height))
            pt2 = (int(xmax * width), int(ymax * height))
            cx = int((xmin + ((xmax - xmin)/ 2)) * width)
            cy = int((ymin + ((ymax - ymin) / 2)) * height)
            if visualise:
                cv2.rectangle(image, pt1, pt2, color=(168, 168, 168),
                              thickness=1)
                cv2.putText(
                    image, name, org=(pt1[0], pt1[1]),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1,
                    color=(255, 0, 0), thickness=2)
            results.append({'box': (pt1 + pt2), 'class_name': name,
                            'centroid_x': cx, 'centroid_y': cy})
        return image, results
