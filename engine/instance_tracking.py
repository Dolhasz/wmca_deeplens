from scipy.spatial import distance
from uuid import uuid1
import numpy as np

class ObjectTracker:
    """
    Object tracking using BGR mean values of bounding boxes.
    After creating an instance, call the update function passing the video
    frame and a set of objects coordinates in the frame for every prediction.
    hold_n_frames specifies for how many frames objects marked as disappeared
    should be kept in memory.
  
        * image is a Numpy array representing the frame.
        * boxes are lists of pixel coordinates, relative to the frame
        dimensions, of detected bounding boxes:
        box = (xmin, ymin, xmax, ymax).
    """
    def __init__(self, hold_n_frames=50):
        self.obj_id = generate_new_id()
        self.registered = dict()
        self.disappeared = dict()
        self.hold_n_frames = hold_n_frames
        self.distance = None

    def register(self, image, obj):
        self.registered[self.obj_id] = get_features(image, obj)
        self.disappeared[self.obj_id] = 0
        self.obj_id = generate_new_id()

    def deregister(self, object_id):
        del self.registered[object_id]
        del self.disappeared[object_id]

    def update(self, image, objects):
        if objects == []: # Case 0: No input objects.
            for key in list(self.registered.keys()):
                self.disappeared[key] += 1
                if self.disappeared[key] >= self.hold_n_frames:
                    self.deregister(key)
            return self.registered

        if len(self.registered) == 0: # Case 1: No registered objects
            for obj in objects:
                self.register(image, obj)

        else: # Case 2: Input objects and registered objects
            inp_objects = {i: get_features(image, obj)
                           for i, obj in enumerate(objects)}
            reg_keys = tuple(self.registered.keys()) # Tuple of registered keys
            # this will keep track of the key names throughout the process.
            input_features = np.array([obj['features']
                                       for obj in inp_objects.values()])
            reg_features = np.array([obj['features']
                                     for obj in self.registered.values()])
            distance_mat = get_dist_mat(input_features, reg_features)
            idx_mat = get_index_mat(distance_mat)

            objs_mapped = set()
            obj_available = set(self.registered.keys())

            for i, block in enumerate(idx_mat):
                # Iterate through all input objects.
                # Every block represents an input object
                if i == reg_features.shape[0]:
                    # Case 3: More input objects than registered:
                    new_objs = set(inp_objects.keys()).difference(objs_mapped)
                    for obj in new_objs:
                        self.registered[self.obj_id] = inp_objects[obj]
                        self.disappeared[self.obj_id] = 0
                        self.obj_id = generate_new_id()
                    break
                input_id = int(block[0, 0]) # Input object ID
                idx = 0
                closest_idx = int(block[1, idx]) # First best match
                while reg_keys[closest_idx] not in obj_available:
                    # get the second best match if the first is already taken.
                    idx += 1
                    closest_idx = int(block[1, idx])
                    self.distance = block[2, idx]
                # Assign the input object to a registered object using the
                # corresponding key.

                self.registered[reg_keys[closest_idx]] = inp_objects[input_id]
                self.disappeared[reg_keys[closest_idx]] = 0
                # self.registered[reg_keys[closest_idx]]['distance'] = self.distance
                obj_available.remove(reg_keys[closest_idx])
                objs_mapped.add(input_id)

            for obj in tuple(obj_available): # Case 4: More registered objects
                # than input objects.
                self.disappeared[obj] += 1
                if self.disappeared[obj] > self.hold_n_frames:
                    self.deregister(obj)
        return self.registered # Return bounding boxes with their IDs.


def get_features(image, obj):
    """ Features and centroids for every object are computed. """
    xmin, ymin, xmax, ymax = obj['box']
    cx = int(xmin + ((xmax - xmin) / 2))
    cy = int(ymin + ((ymax - ymin) / 2))
    region = image[ymin:ymax, xmin:xmax, :]
    c_mean = np.mean(region, axis=(0, 1)) / 255 # Normalised features
    position = np.array([cx / image.shape[1], cy / image.shape[0]])
    features = np.concatenate((c_mean, position))
    return {'features': features, 'class_name': obj['class_name'],
            'centroid_x': cx, 'centroid_y': cy, 'region': obj['region']}


def get_dist_mat(input_features, reg_features):
    """ Create a distance matrix where each cell indicates the distance
    between colour mean points of input and registered objects in the
    euclidean space. """
    # objects to compare
    mat = np.zeros((len(input_features), len(reg_features)))
    for i, row in enumerate(mat):
        for j, _ in enumerate(row):
            mat[i, j] = distance.euclidean(
                input_features[i], reg_features[j])
    return mat


def get_index_mat(distance_mat):
    """ Numpy array which specifies which are the closest objects in the
    distance matrix. """
    idx = np.zeros((distance_mat.shape[0], 3, distance_mat.shape[1]))
    for i, row in enumerate(distance_mat):
        idx[i, 0, 0] = i            # Object ID
        idx[i, 1] = np.argsort(row) # Closest indices
        idx[i, 2] = np.sort(row)    # Corresponding values
    return idx[np.argsort(idx[:, 2, 0])] # Sort ndarray by distances


def generate_new_id():
    """ Returns the first sequence of a generated uuid. """
    return str(uuid1())[:8]
