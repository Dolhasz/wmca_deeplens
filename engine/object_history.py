""" Keep track of position of detected objects over time. """
import threading
import queue
import os
import csv
from datetime import datetime


class History(threading.Thread):
    """ Object containing a pandas DataFrame indicating object positions
    across frames. """
    def __init__(self, h_queue):
        columns = ['Frame', 'Object ID', 'Centroid X', 'Centroid Y', 'Class',
                   'Time', 'Region']
        self.columns = columns
        self.tmp = './_hist_data.csv'
        self._write_headers()
        self.nframe = 0
        self.queue = h_queue
        self.done = False

        self.stop_request = threading.Event()
        self.lock = threading.Lock()
        super().__init__()

    def run(self):
        while not self.stop_request.is_set():
            try:
                data = self.queue.get(block=True, timeout=3)
            except queue.Empty as _:
                continue
            self.update(data)
            self.queue.task_done()

        while not self.queue.empty(): # Process any elements left in the queue
            try:
                data = self.queue.get(block=True, timeout=1)
            except queue.Empty as _:
                break
            self.update(data)
            self.queue.task_done()
        self.done = True

    def update(self, objects):
        """ Call this methods for every set of object centroids calculated
        from the model. """
        rows = list()
        time = datetime.now().strftime('%Y,%m,%d - %H:%M:%S')
        for key, obj in objects.items():
            name = obj['class_name']
            region = obj['region']
            cx = int(obj['centroid_x'])
            cy = int(obj['centroid_y'])
            rows.append([self.nframe, key, cx, cy, name, time, region])
        with open(self.tmp, 'a') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerows(rows)
        self.nframe += 1

    def save_data(self, path):
        if not self.done:
            print('[ERROR]: Something went wrong when writing the data!')
        os.rename(self.tmp, path)

    def discard_data(self):
        os.remove(self.tmp)

    def _write_headers(self):
        with open(self.tmp, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(self.columns)
