"""
Visualise inference from both the DeepLens and Rekognition by displaying
bounding boxes on frames.
"""
import json
from math import sqrt
import numpy as np
import matplotlib.path as path
import cv2


def visualise_count(frame, hist):
    cv2.putText(
        img=frame,
        text='Vehicle count: {}'.format(len(hist.unique_count())),
        org=(frame.shape[1] - 580, frame.shape[0] - 40),
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=2,
        color=(255, 255, 255),
        thickness=2)
    return frame


def visualise_tracking(image, objects):
    for key, box in objects.items():
        org = (int(box['centroid_x']), int(box['centroid_y']))
        cv2.putText(
            img=image,
            text='ID: {}'.format(key),
            org=tuple([org[0] + 10, org[1] - 10]),
            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            fontScale=1,
            color=(250, 250, 0),
            thickness=2)
        cv2.circle(img=image, center=org, radius=10, color=(250, 250, 0),
                   thickness=-1)
    return image


def visualise_directions(frame, hist, centroids, n_frames=15):
    """ This function draws a segment according to the object positions for 
    a specified timeframe. """
    hists = hist.last_n_instances(centroids.keys(), n_frames)
    obj_ids = hists['Object ID'].unique().tolist()
    for obj in obj_ids:
        df = hists.loc[(hists['Object ID'] == obj)] # Get history of an object
        centr = df.loc[df['Frame'] == df['Frame'].min()] # Get past frame
        pt1 = (int(centr['Centroid X']), int(centr['Centroid Y']))
        pt2 = (int(centroids[obj]['centroid_x']),
               int(centroids[obj]['centroid_y']))

        distance = sqrt(# Get cartesian distance from both instances.
            (centroids[obj]['centroid_x'] - centr['Centroid X']) ** 2
            + (centroids[obj]['centroid_y'] - centr['Centroid Y']) ** 2)
        if distance < 180:
            cv2.arrowedLine(img=frame, pt1=pt1, pt2=pt2,
                            color=(26, 209, 255), thickness=2)
    return frame


class Interface:
    def __init__(self, w_name='Window_0', regions_path=None):
        self.wname = w_name
        self.current = list()
        self.regions = dict()
        self.key = 0
        self.paths = dict()
        if regions_path is not None:
            with open(regions_path, 'r') as file:
                regions = json.load(file)
            self.set_regions(regions)

    def create_window(self):
        cv2.namedWindow(self.wname)
        cv2.setMouseCallback(self.wname, self.on_mouse)

    def on_mouse(self, event, x, y, flags, userdata):
        if event == cv2.EVENT_LBUTTONDOWN: # Mouse pressed
            if len(self.current) < 4:
                self.current.append([x, y])
                if len(self.current) == 4:
                    self.regions[self.key] = [self.current.pop(0)for _
                                              in range(len(self.current))]
                    self.key += 1
                    self.set_paths()

    def update(self, image, keypress):
        if keypress == ord('c'):
            if len(self.regions.keys()) != 0:
                self.key -= 1
                self.regions.pop(self.key)
                self.paths.pop(self.key)
        if keypress == ord('s'):
            self.save_regions()

        for point in self.current:
            cv2.circle(img=image, center=tuple(point), radius=10,
                color=(10, 10, 10), thickness=-1)
        if len(self.regions) > 0:
            for key, pts in self.regions.items():
                arr = np.array([pts])
                xmin, ymin = min(pts)
                cv2.polylines(img=image, pts=arr, isClosed=True,
                    color=(255, 255, 128), thickness=2)
                cv2.putText(img=image, text='region: {}'.format(key + 1),
                    org=(xmin + 10, ymin + 10),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1,
                    thickness=2, color=(255, 255, 255))
        return image

    def objs_in_regions(self, objects):
        """ Returns True if the input coordinates are within the regions
        defined.
        * objects must be a dict
        """
        in_region = objects.copy()
        for key, obj in enumerate(in_region):
            point = tuple([obj['centroid_x'], obj['centroid_y']])
            if any([self.paths[i].contains_point(point)
                    for i in self.paths.keys()]):
                for i, region in self.paths.items():
                    if region.contains_point(point):
                        in_region[key]['region'] = i
            else:
                in_region[key]['region'] = -1
        return in_region

    def save_regions(self):
        regions = self.regions.copy()
        with open('./regions.json', 'w') as file:
            json.dump(regions, file)
            
    def set_regions(self, regions):
        for region in regions.values():
            self.regions[self.key] = region
            self.key += 1
        self.set_paths()

    def set_paths(self):
        self.paths = {i: path.Path(self.regions[i])
                      for i in self.regions.keys()}
