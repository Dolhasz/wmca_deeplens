""" Ensure that the docker container is started properly before executing
this file.
"""
import argparse
import os
import json
from datetime import datetime
import queue
from progress.bar import Bar
import cv2
import engine



def infer(video_file, data_path=None, display=True, pre_def_regions=None,
         url='localhost', port=8500, confidence=0.65, n_frames=20,
         res=(600, 600)):
    hist_q = queue.Queue()
    history = engine.History(h_queue=hist_q)
    history.start()
    tracker = engine.ObjectTracker(hold_n_frames=n_frames)
    interface = engine.Interface(regions_path=pre_def_regions)
    if display:
        interface.create_window()
    client = engine.GRPCClient(url=url, port=port, confidence=confidence)

    cap = cv2.VideoCapture(video_file)
    frame_info = get_frame_info(cap)
    print('[INFO]: Starting inference.')

    p_bar = Bar('Video processing', max=frame_info['frame_count'],
                suffix='%(percent)d%%')
    time_0 = datetime.now().strftime('%Y, %m, %D - %H:%M:%S')

    REGION_A = set()
    REGION_B = set()

    keypress = 0
    while keypress != ord('q') and cap.isOpened():
        p_bar.next()
        ret, frame = cap.read()
        if not ret:
            break
        # frame = cv2.resize(frame, res)
        preds = client.predict(frame)
        image, results = client.process_boxes(frame, preds, display)
        objs = interface.objs_in_regions(results)
        tracked = tracker.update(image, objs)
        hist_q.put(tracked)

        AB_intersection = intersect(tracked, REGION_A, REGION_B)

        if display:
            image = interface.update(image, keypress)
            image = engine.visualise_tracking(image, tracked)

            cv2.imshow(interface.wname, image)
            keypress = cv2.waitKey(1)

    p_bar.finish()
    print('[INFO]: Halting inference.')
    history.stop_request.set()
    cap.release()
    cv2.destroyAllWindows()

    counts = {'Region_A': list(REGION_A),
              'REGION_B': list(REGION_B),
              'A_AND_B': list(AB_intersection)}
    save_data(video_file, data_path, history, interface, frame_info, time_0,
              confidence, res, n_frames, counts)

def intersect(objs, A, B):
    for key, value in objs.items():
        if value['region'] == 0:
            A.add(key)
        elif value['region'] == 1:
            B.add(key)
    return A.intersection(B)


def get_frame_info(cap):
    frame_info = {'fps': cap.get(cv2.CAP_PROP_FPS),
                  'frame_count': cap.get(cv2.CAP_PROP_FRAME_COUNT),
                  'frame_height': cap.get(cv2.CAP_PROP_FRAME_HEIGHT),
                  'frame_width': cap.get(cv2.CAP_PROP_FRAME_WIDTH)}
    return frame_info

def save_data(video_file, data_path, history, interface, video_info, s_time,
              confidence, resolution, n_frames, counts):
    file_name = str(os.path.basename(video_file))
    folder = file_name.split('.')[0]
    path_is_valid = False

    while not path_is_valid:
        if data_path is None:
            ans = 0
            while ans not in ['y', 'n', 'yes', 'no']:
                ans = input('Save data before exiting? (yes, no): ')
            if ans in ['y', 'yes']:
                data_path = input('Output path: ')
            else:
                history.discard_data()
                break
        results_path = os.path.join(os.getcwd(), data_path, folder)
        history_path = os.path.join(results_path, 'history.csv')

        if not os.path.isdir(results_path):
            try:
                os.mkdir(results_path)
            except OSError as err:
                if err.errno == 2:
                    print('[ERROR]: That was far from being a valid path :(')
                    print('Try again...')
                    data_path = input('Output path: ')
        else:
            path_is_valid = True

    if path_is_valid:
        info = {'video_file': os.path.abspath(video_file),
                'frame_info': video_info,
                'regions': {
                    'region_{}'.format(str(i)):  interface.regions[i]
                    for i in interface.regions.keys()},
                'history_path': history_path,
                'start_time': s_time,
                'end_time': datetime.now().strftime('%Y, %m, %D - %H:%M:%S'),
                'n_history_frame': n_frames,
                'prediction_confidence': confidence,
                'input_resolution': resolution,
                'counts': counts}

        print('[INFO]: Saving csv file...')
        if not history.done:
            history.join()
        history.save_data(history_path)
        print('[INFO]: Saving additional info...')
        with open(os.path.join(results_path, 'info.json'), 'w') as file:
            json.dump(info, file)
    print('[INFO]: Done :D Exiting.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('videofile', type=str, help='Video path')
    parser.add_argument('--output', type=str)
    parser.add_argument('--regions', type=str)
    parser.add_argument('--url', type=str, required=False, default='localhost')
    parser.add_argument('--confidence', type=float, default=0.65)
    parser.add_argument('--n-frames', type=int, default=20)
    args = parser.parse_args()

    assert (args.confidence < 1.0 and args.confidence > 0)

    visualise = True

    infer(args.videofile, args.output, visualise, args.regions, url=args.url,
          confidence=args.confidence, n_frames=args.n_frames)
