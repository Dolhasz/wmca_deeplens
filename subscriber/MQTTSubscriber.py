import ssl
import json
import paho.mqtt.client as paho


class MQTTSubscriber:
    """ 
    Argument vars_file is used to set MQTT topic, paths, certificate
    files and keys.
    Method 'on_msg' needs to have the following arguments:
    client, userdata, flags, msg
    Access data using msg.payload
    """

    def __init__(self, vars_file, on_msg, msg_queue):
        with open(vars_file, 'r') as f:
            self.mqtt_vars = json.load(f)
        client = paho.Client(userdata=msg_queue)
        client.on_connect = self.on_connect
        client.on_message = on_msg
        client.tls_set(
            ca_certs=self.mqtt_vars['ROOT_CA'],
            certfile=self.mqtt_vars['CRT'],
            keyfile=self.mqtt_vars['PRIVATE_KEY'],
            cert_reqs=ssl.CERT_REQUIRED,
            tls_version=ssl.PROTOCOL_TLSv1_2,
            ciphers=None)
        client.connect(
            self.mqtt_vars['AWS_ENDPOINT'],
            self.mqtt_vars['AWS_PORT'],
            keepalive=60)
        self.client = client

    def on_connect(self, client, userdata, flags, rc):
        if int(rc) == 0:
            print('Client connected!')
            client.subscribe(self.mqtt_vars['TOPIC'], 1)
