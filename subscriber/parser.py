import json
import base64
import numpy as np
import cv2


def decode_image(image_bytes):
    try:
        bytes_array = base64.b64decode(image_bytes)
    except base64.binascii.Incomplete as exc:
        print('Could not decode Image... Error: {}'.format(exc))
        raise
    bytes_array = np.frombuffer(bytes_array, dtype=np.uint8)
    return cv2.imdecode(bytes_array, cv2.IMREAD_COLOR)


def decode_message(raw_json):
    try:
        data = json.loads(raw_json)
    except json.JSONDecodeError as exc:
        print('Could not parse message from MQTT. Error: {}'.format(exc))
        raise
    return {'DeepLens': data['DL_Inference'],
            'Image': decode_image(data['Image']), 'Time': data['Time']}


def parse_dl_objects(data):
    objs = list()
    try:
        inference = json.loads(data['DeepLens'])
    except json.JSONDecodeError as exc:
        print(exc)
        raise
    for name, label in inference['Labels'].items():
        for obj_inst in label:
            bbox = obj_inst['BoundingBox']
            # Get centroids
            cx = int(bbox['Left'] + bbox['Width'] / 2)
            cy = int(bbox['Top'] + bbox['Height'] / 2)
            objs.append({'obj': name, 'centroid_x': cx, 'centroid_y': cy,
                         'time': data['Time']})
    return objs


def parse_rk_objects(data):
    objs = list()
    for label in data['Rekognition']['Labels']:
        name = label['Name']
        for obj_inst in label['Instances']:
            bbox = obj_inst['BoundingBox']
            # Get centroids
            cx = int(bbox['Left'] + bbox['Width'] / 2)
            cy = int(bbox['Top'] + bbox['Height'] / 2)
            objs.append({'obj': name, 'centroid_x': cx, 'centroid_y': cy,
                         'time': data['Time']})
    return objs
